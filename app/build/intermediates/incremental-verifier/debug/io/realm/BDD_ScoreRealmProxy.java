package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import com.example.herouard.piedpiper.BDD_Score;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BDD_ScoreRealmProxy extends BDD_Score
    implements RealmObjectProxy {

    static final class BDD_ScoreColumnInfo extends ColumnInfo {

        public final long scoreIndex;
        public final long idIndex;

        BDD_ScoreColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(2);
            this.scoreIndex = getValidColumnIndex(path, table, "BDD_Score", "score");
            indicesMap.put("score", this.scoreIndex);

            this.idIndex = getValidColumnIndex(path, table, "BDD_Score", "id");
            indicesMap.put("id", this.idIndex);

            setIndicesMap(indicesMap);
        }
    }

    private final BDD_ScoreColumnInfo columnInfo;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("score");
        fieldNames.add("id");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    BDD_ScoreRealmProxy(ColumnInfo columnInfo) {
        this.columnInfo = (BDD_ScoreColumnInfo) columnInfo;
    }

    @Override
    @SuppressWarnings("cast")
    public int getScore() {
        realm.checkIfValid();
        return (int) row.getLong(columnInfo.scoreIndex);
    }

    @Override
    public void setScore(int value) {
        realm.checkIfValid();
        row.setLong(columnInfo.scoreIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int getId() {
        realm.checkIfValid();
        return (int) row.getLong(columnInfo.idIndex);
    }

    @Override
    public void setId(int value) {
        realm.checkIfValid();
        row.setLong(columnInfo.idIndex, value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_BDD_Score")) {
            Table table = transaction.getTable("class_BDD_Score");
            table.addColumn(ColumnType.INTEGER, "score", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.INTEGER, "id", Table.NOT_NULLABLE);
            table.addSearchIndex(table.getColumnIndex("id"));
            table.setPrimaryKey("id");
            return table;
        }
        return transaction.getTable("class_BDD_Score");
    }

    public static BDD_ScoreColumnInfo validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_BDD_Score")) {
            Table table = transaction.getTable("class_BDD_Score");
            if (table.getColumnCount() != 2) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 2 but was " + table.getColumnCount());
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for (long i = 0; i < 2; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final BDD_ScoreColumnInfo columnInfo = new BDD_ScoreColumnInfo(transaction.getPath(), table);

            if (!columnTypes.containsKey("score")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'score' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("score") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'score' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.scoreIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'score' does support null values in the existing Realm file. Use corresponding boxed type for field 'score' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("id") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'id' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.idIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'id' does support null values in the existing Realm file. Use corresponding boxed type for field 'id' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'id' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("id"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The BDD_Score class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_BDD_Score";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static BDD_Score createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        BDD_Score obj = null;
        if (update) {
            Table table = realm.getTable(BDD_Score.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("id")) {
                long rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("id"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new BDD_ScoreRealmProxy(realm.getColumnInfo(BDD_Score.class));
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            obj = realm.createObject(BDD_Score.class);
        }
        if (json.has("score")) {
            if (json.isNull("score")) {
                throw new IllegalArgumentException("Trying to set non-nullable field score to null.");
            } else {
                obj.setScore((int) json.getInt("score"));
            }
        }
        if (json.has("id")) {
            if (json.isNull("id")) {
                throw new IllegalArgumentException("Trying to set non-nullable field id to null.");
            } else {
                obj.setId((int) json.getInt("id"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    public static BDD_Score createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        BDD_Score obj = realm.createObject(BDD_Score.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("score")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field score to null.");
                } else {
                    obj.setScore((int) reader.nextInt());
                }
            } else if (name.equals("id")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field id to null.");
                } else {
                    obj.setId((int) reader.nextInt());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static BDD_Score copyOrUpdate(Realm realm, BDD_Score object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        BDD_Score realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(BDD_Score.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = table.findFirstLong(pkColumnIndex, object.getId());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new BDD_ScoreRealmProxy(realm.getColumnInfo(BDD_Score.class));
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static BDD_Score copy(Realm realm, BDD_Score newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        BDD_Score realmObject = realm.createObject(BDD_Score.class, newObject.getId());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setScore(newObject.getScore());
        realmObject.setId(newObject.getId());
        return realmObject;
    }

    static BDD_Score update(Realm realm, BDD_Score realmObject, BDD_Score newObject, Map<RealmObject, RealmObjectProxy> cache) {
        realmObject.setScore(newObject.getScore());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("BDD_Score = [");
        stringBuilder.append("{score:");
        stringBuilder.append(getScore());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{id:");
        stringBuilder.append(getId());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BDD_ScoreRealmProxy aBDD_Score = (BDD_ScoreRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aBDD_Score.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aBDD_Score.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aBDD_Score.row.getIndex()) return false;

        return true;
    }

}
